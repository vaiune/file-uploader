using FlyingDonkeyFileUploader.Application.Common.Mappings;
using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Application.Files.ViewModels;

public class FileRequestDto : IMapFrom<File>
{
    public string Name { get; init; }
    public string MimeType { get; init; }
    public string Extension { get; init; }
    public long Size { get; init; }
    public byte[] Content { get; init; }
}