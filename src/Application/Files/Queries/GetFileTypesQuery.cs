using FlyingDonkeyFileUploader.Application.Common.Interfaces;
using FlyingDonkeyFileUploader.Application.Files.ViewModels;
using MediatR;

namespace FlyingDonkeyFileUploader.Application.Files.Queries;

public class GetFileTypesQuery : IRequest<IEnumerable<FileTypeDto>> { }

public class GetFileTypesQueryHandler : IRequestHandler<GetFileTypesQuery, IEnumerable<FileTypeDto>>
{
    private readonly IFileService _fileService;

    public GetFileTypesQueryHandler(IFileService fileService)
    {
        _fileService = fileService;
    }

    public async Task<IEnumerable<FileTypeDto>> Handle(GetFileTypesQuery request, CancellationToken cancellationToken)
    {
        return await _fileService.GetFileTypes(cancellationToken);
    }
}