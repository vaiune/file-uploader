using FlyingDonkeyFileUploader.Application.Common.Models;
using FlyingDonkeyFileUploader.Application.Files.Queries;
using FlyingDonkeyFileUploader.Application.Files.ViewModels;

namespace FlyingDonkeyFileUploader.Application.Common.Interfaces;

public interface IFileService
{
    Task<IEnumerable<FileResponseDto>> GetAllFiles(CancellationToken ct);

    Task<PaginatedList<FileResponseDto>> GetPaginatedFiles(GetFilesPaginatedByTypeQuery query);

    Task<int> AddFile(FileRequestDto fileRequest, CancellationToken ct);

    Task<FileResultDto> GetFileById(int id);

    Task<IEnumerable<FileTypeDto>> GetFileTypes(CancellationToken ct);

    Task DeleteFile(int id, CancellationToken ct);
}