import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from './store.state';
 
export const selectState = createFeatureSelector<AppState>('state');

export const selectFiles = createSelector(
  selectState,
  (state: AppState) => state.files
);

export const selectTotalCount = createSelector(
  selectState,
  (state: AppState) => state.totalCount
);

export const selectExtensions = createSelector(
  selectState,
  (state: AppState) => state.extensions
);

export const selectUserState = createSelector(
  selectState,
  (state: AppState) => state.userState
);

export const selectError = createSelector(
  selectState,
  (state: AppState) => state.error
);
