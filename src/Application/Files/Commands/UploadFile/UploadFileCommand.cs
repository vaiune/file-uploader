using FlyingDonkeyFileUploader.Application.Common.Interfaces;
using FlyingDonkeyFileUploader.Application.Files.ViewModels;
using MediatR;

namespace FlyingDonkeyFileUploader.Application.Files.Commands.UploadFile;

public class UploadFileCommand : IRequest<int>
{
    public FileRequestDto FileRequest { get; init; }
}

public class UploadFileCommandHandler : IRequestHandler<UploadFileCommand, int>
{
    private readonly IFileService _fileService;

    public UploadFileCommandHandler(IFileService fileService)
    {
        _fileService = fileService;
    }

    public async Task<int> Handle(UploadFileCommand request, CancellationToken cancellationToken)
    {
        return await _fileService.AddFile(request.FileRequest, cancellationToken);
    }
}