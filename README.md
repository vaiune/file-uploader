## File Uploader

.NET 6 Web API + Angular

#### .NET 6 Web API
##### src folder has the API solution which consists of 4 projects

- Domain
- Application
- Infrastructure
- API

It has CQRS pattern implemented and uses Sqlite via EF Core for persistence

To add a new migration simply run these in terminal in the root directory of the repository

```
// adding a new migration
dotnet ef migrations add "initial" --project src/Infrastructure --startup-project src/API --output-dir Persistence/Migrations

// updating the database using migrations 
// (no need to run explicitly here, it applies migrations itself at the start)
dotnet ef database update --project src/Infrastructure --startup-project src/API 
```

You can start the API in the root directory via terminal 

```
dotnet run --project src/API 
```
This will spawn a process on port 5001, you can go to [localhost:5001](http://localhost:5001/swagger) for Swagger API Documentation
##### tests folder has an unit test and an integration test projects

You can run the test via terminal in the root directory of the repository

```
dotnet test
```

#### Angular Frontend

AngularClient folder has the angular project

You should first install the node packages

```
npm install
```

You can run the tests via terminal in the AngularClient folder

```
npm test
```

You can start the angular application via terminal in the AngularClient folder

```
npm start
```
This will spawn a process on port 4200, you can go to [localhost:4200](http://localhost:4200) for Angular