import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { environment } from "src/environments/environment";
import { ExtensionModel } from "../models/extension.model";
import { FileModel } from "../models/file.model";
import { PaginationModel } from "../models/pagination.model";

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private httpClient: HttpClient) {
  }

  getFiles(pageNumber: number, pageSize: number, extension: string) {
    return this.httpClient.get<PaginationModel<FileModel>>(environment.apiUrl + '/api/files', {
      params: {
        pageNumber,
        pageSize,
        extension
      }
    });
  }
  
  getExtensions() {
    return this.httpClient.get<ExtensionModel[]>(environment.apiUrl +'/api/files/getFileTypes');
  }

  upload(file: File) {
    const formData = new FormData();
    formData.append('file', file, file.name)
    return this.httpClient.post(environment.apiUrl + '/api/files', formData)
  }

  downloadFile(id: number): Observable<Blob>{
    return this.httpClient.get<Blob>(environment.apiUrl + '/api/files/download/' + id, {responseType: 'blob' as 'json'})
  }

  delete(id: number) {
    return this.httpClient.delete(environment.apiUrl + '/api/files/delete/' + id)
  }

  /*
  getFiles(pageNumber: number, pageSize: number, extension: string): Observable<PaginationModel<FileModel>> {
    return of(
      {
        "pageNumber": 1,
        "totalPages": 1,
        "totalCount": 4,
        "items": [
            {
                "id": 1,
                "name": "BA27F299-E4F9-4FF3-BB04-F1A10D7253FE.JPG",
                "mimeType": "image/jpeg",
                "extension": ".jpg",
                "size": 996975,
                "created": "2022-08-30T18:15:59.028719Z"
            },
            {
                "id": 2,
                "name": "AsiTakvimi.pdf",
                "mimeType": "application/pdf",
                "extension": ".pdf",
                "size": 112153,
                "created": "2022-08-30T18:16:03.125283Z"
            },
            {
                "id": 3,
                "name": "civicVava.pdf",
                "mimeType": "application/pdf",
                "extension": ".pdf",
                "size": 13215678,
                "created": "2022-08-30T18:16:09.217815Z"
            },
            {
                "id": 4,
                "name": "cta.html",
                "mimeType": "text/html",
                "extension": ".html",
                "size": 18028,
                "created": "2022-08-30T18:16:14.796527Z"
            }
        ]
    }
    )
  }

  getExtensions(): Observable<ExtensionModel[]> {
    return of([
      {
        "extension": ".jpg",
        "count": 1
      },
      {
        "extension": ".pdf",
        "count": 2
      },
      {
        "extension": ".html",
        "count": 1
      }
    ])
  }*/
}