using FlyingDonkeyFileUploader.Application.Common.Interfaces;
using FlyingDonkeyFileUploader.Application.Common.Models;
using FlyingDonkeyFileUploader.Application.Files.ViewModels;
using MediatR;

namespace FlyingDonkeyFileUploader.Application.Files.Queries;

public class GetFilesPaginatedByTypeQuery : IRequest<PaginatedList<FileResponseDto>>
{
    public string? Extension { get; init; } = string.Empty;
    public int PageNumber { get; init; } = 1;
    public int PageSize { get; set; } = 10;
}

public class GetFilesPaginatedByTypeQueryHandler : IRequestHandler<GetFilesPaginatedByTypeQuery, PaginatedList<FileResponseDto>>
{
    private readonly IFileService _fileService;

    public GetFilesPaginatedByTypeQueryHandler(IFileService fileService)
    {
        _fileService = fileService;
    }

    public async Task<PaginatedList<FileResponseDto>> Handle(GetFilesPaginatedByTypeQuery request, CancellationToken cancellationToken)
    {
        return await _fileService.GetPaginatedFiles(request);
    }
}