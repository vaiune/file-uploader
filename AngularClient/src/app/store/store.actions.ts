import { createAction, props } from '@ngrx/store';
import { ExtensionModel } from '../models/extension.model';
import { FileModel } from '../models/file.model';
import { PaginationModel } from '../models/pagination.model';

export const setPage = createAction('[File] Set Page', props<{ page: number }>())
export const setExtension = createAction('[File] Set Extension', props<{ extension: string }>())

export const loadFiles = createAction('[Files] Load')
export const loadFilesCompleted = createAction('[Files] Load Completed', props<{ payload: PaginationModel<FileModel> }>())

export const loadExtensions = createAction('[Files] Load Extensions');
export const loadExtensionsCompleted = createAction('[Files] Load Extensions Completed', props<{ payload: ExtensionModel[] }>())

export const uploadFile = createAction('[Upload]', props<{ file: File }>())
export const deleteFile = createAction('[Delete]', props<{ id: number }>())

export const uploadFileCompleted = createAction('[Upload] Completed')


export const catchNetworkError = createAction('[Network] Catch Error', props<{ e: any }>())