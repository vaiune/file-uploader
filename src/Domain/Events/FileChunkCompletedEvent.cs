using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Domain.Events;

public class FileChunkCompletedEvent : BaseEvent
{
    public FileChunkCompletedEvent(File file)
    {
        File = file;
    }

    public File File { get; }
}