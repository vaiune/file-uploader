namespace FlyingDonkeyFileUploader.Domain.Exceptions;

public class ChunkedFileException : Exception
{
    public ChunkedFileException(string name)
        : base($"Chunked file \"{name}\" is corrupted.")
    {
    }
}