import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Observable, Subscription } from 'rxjs';
import { ExtensionModel } from '../models/extension.model';
import { FileModel } from '../models/file.model';
import { FileService } from '../services/file.service';
import { deleteFile, loadExtensions, loadFiles, setExtension, setPage } from '../store/store.actions';
import { selectExtensions, selectFiles, selectTotalCount, selectUserState } from '../store/store.selectors';
import { AppState, UserState } from '../store/store.state';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit, OnDestroy {
  files: Observable<FileModel[]>;
  extensions: Observable<ExtensionModel[]>;

  subscriptions: Subscription[] = [];
  totalCount: number;

  userState: UserState = {
    currentPage: 1,
    pageSize: 10,
    extension: ''
  }

  constructor(private store: Store<{ state: AppState }>, private fileService: FileService) {
    this.files = store.select(selectFiles);
    this.extensions = store.select(selectExtensions);
    this.subscriptions.push(store.select(selectTotalCount).subscribe(totalCount => this.totalCount = totalCount));
    this.subscriptions.push(store.select(selectUserState).subscribe(userState => this.userState = userState));
  }

  ngOnInit(): void {
    this.load();
    this.loadExtensions();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      if (!sub.closed)
        sub.unsubscribe();
    })
  }

  load() {
    this.store.dispatch(loadFiles())
  }

  loadExtensions() {
    this.store.dispatch(loadExtensions())
  }

  pageChanged(e: PageChangedEvent) {
    this.store.dispatch(setPage({ page: e.page }))
  }

  selectExtension(ext: ExtensionModel) {
    this.store.dispatch(setExtension({ extension: ext.extension }))
  }

  deleteFile(id: number){
    this.store.dispatch(deleteFile({ id: id}))
  }

  download(file: FileModel){
    this.fileService.downloadFile(file.id).subscribe(response => {
      let dataType = response.type;
      let binaryData = [];
      binaryData.push(response);
      let downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
      downloadLink.setAttribute('download', file.name);
      document.body.appendChild(downloadLink);
      downloadLink.click();
    })
  }
}
