import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectError } from '../store/store.selectors';
import { AppState } from '../store/store.state';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  error: Observable<any>;

  constructor(store: Store<{ state: AppState }>) {
    this.error = store.select(selectError);
  }

  ngOnInit(): void {
  }

}
