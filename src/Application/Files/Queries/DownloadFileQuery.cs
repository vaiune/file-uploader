using FlyingDonkeyFileUploader.Application.Common.Interfaces;
using FlyingDonkeyFileUploader.Application.Files.ViewModels;
using MediatR;

namespace FlyingDonkeyFileUploader.Application.Files.Queries;

public class DownloadFileQuery : IRequest<FileResultDto> 
{
    public int Id { get; init; }
}

public class DownloadFileQueryHandler : IRequestHandler<DownloadFileQuery, FileResultDto>
{
    private readonly IFileService _fileService;

    public DownloadFileQueryHandler(IFileService fileService)
    {
        _fileService = fileService;
    }
    
    public async Task<FileResultDto> Handle(DownloadFileQuery request, CancellationToken cancellationToken)
    {
        return await _fileService.GetFileById(request.Id);
    }
}