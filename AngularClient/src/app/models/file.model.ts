export interface FileModel {
  id: number,
  name: string,
  mimeType: string,
  extension: string,
  size: number,
  created: string
}