import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { of } from 'rxjs';
import { selectFiles } from '../store/store.selectors';
import { AppState } from '../store/store.state';
import { FileComponent } from './file.component';
import { FileSizePipe } from '../pipes/file-size.pipe';

const initialState: AppState = {
  files: [],
  pageNumber: 0,
  totalCount: 0,
  totalPages: 0,

  userState: {
    currentPage: 1,
    extension: '',
    pageSize: 10
  },

  extensions: [],
  error: undefined,

  isLoading: false
}

const mockFileData = [
  {
      "id": 1,
      "name": "BA27F299-E4F9-4FF3-BB04-F1A10D7253FE.JPG",
      "mimeType": "image/jpeg",
      "extension": ".jpg",
      "size": 996975,
      "created": "2022-08-30T18:15:59.028719Z"
  },
  {
      "id": 2,
      "name": "AsiTakvimi.pdf",
      "mimeType": "application/pdf",
      "extension": ".pdf",
      "size": 112153,
      "created": "2022-08-30T18:16:03.125283Z"
  },
  {
      "id": 3,
      "name": "civicVava.pdf",
      "mimeType": "application/pdf",
      "extension": ".pdf",
      "size": 13215678,
      "created": "2022-08-30T18:16:09.217815Z"
  },
  {
      "id": 4,
      "name": "cta.html",
      "mimeType": "text/html",
      "extension": ".html",
      "size": 18028,
      "created": "2022-08-30T18:16:14.796527Z"
  }
];

describe('FileComponent', () => {
  let component: FileComponent;
  let fixture: ComponentFixture<FileComponent>;
  let store: MockStore;
  let mockSelector: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileComponent, FileSizePipe ],
      imports: [
        PaginationModule.forRoot(),
        [HttpClientTestingModule]
      ],
      providers: [
        provideMockStore({ initialState })
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
  });

  it('should create', () => {
    fixture = TestBed.createComponent(FileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should render files', () => {
    mockSelector = store.overrideSelector(selectFiles, mockFileData);
    fixture = TestBed.createComponent(FileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
    mockFileData.forEach(f => {
      const element = fixture.nativeElement.querySelector("#file_" + f.id);

      expect(element).toBeTruthy();
    });

    const files = fixture.nativeElement.querySelectorAll("tr[id^='file_']")
    expect(files).toHaveSize(mockFileData.length);
  })

  it('should not render files', () => {
    mockSelector = store.overrideSelector(selectFiles, []);
    fixture = TestBed.createComponent(FileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
    const files = fixture.nativeElement.querySelectorAll("tr[id^='file_']")
    
    expect(files).toHaveSize(0);
  })
});
