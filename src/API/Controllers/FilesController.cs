using FluentValidation.Results;
using FlyingDonkeyFileUploader.Application;
using FlyingDonkeyFileUploader.Application.Common.Exceptions;
using FlyingDonkeyFileUploader.Application.Common.Models;
using FlyingDonkeyFileUploader.Application.Files.Commands;
using FlyingDonkeyFileUploader.Application.Files.Commands.UploadFile;
using FlyingDonkeyFileUploader.Application.Files.Queries;
using FlyingDonkeyFileUploader.Application.Files.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class FilesController : ApiControllerBase
{
    [HttpGet]
    public async Task<PaginatedList<FileResponseDto>> GetAllFiles([FromQuery] GetFilesPaginatedByTypeQuery query, CancellationToken cancellationToken)
    {
        return await Mediator.Send(query, cancellationToken);
    }
    
    [HttpGet("/api/Files/GetFileTypes")]
    public async Task<IEnumerable<FileTypeDto>> GetFileTypes(CancellationToken cancellationToken)
    {
        return await Mediator.Send(new GetFileTypesQuery(), cancellationToken);
    }

    [HttpPost]
    public async Task<ActionResult<int>> UploadFile(IFormFile file, CancellationToken cancellationToken)
    {
        var validationFailures = new List<ValidationFailure>();
        if (file.Length == 0)
        {  
            validationFailures.Add(new ValidationFailure("Size", "File size must be greater than zero."));
        }  
  
        // if (!file.ContentType.Contains("image"))  
        // {  
        //     validationFailures.Add(new ValidationFailure("ContentType", "Uploaded Content-Type doesn't meet the requirements."));
        // }  
  
        string uploadedFileName = file.FileName;

        if (validationFailures.Any())
            throw new ValidationException(validationFailures);

        using var ms = new MemoryStream();
        await file.CopyToAsync(ms, cancellationToken);
        var bytes = ms.ToArray();
        
        var fileDto = new FileRequestDto
        {
            Content = bytes,
            Size = bytes.Length,
            Name = uploadedFileName,
            Extension = Path.GetExtension(uploadedFileName).ToLower(),
            MimeType = MimeTypes.GetMimeType(uploadedFileName)
        };

        return await Mediator.Send(new UploadFileCommand {FileRequest = fileDto}, cancellationToken);
    }

    [HttpGet("/api/Files/Download/{Id:int}")]
    public async Task<IActionResult> DownloadFile(int id, CancellationToken cancellationToken)
    {
        var query = new DownloadFileQuery{ Id = id };
        var fileResult = await Mediator.Send(query, cancellationToken);

        if (fileResult.Successful)
            return File(fileResult.Content, fileResult.MimeType, fileResult.Name);
        else
            return NotFound();
    }

    [HttpDelete("/api/Files/Delete/{id:int}")]
    public async Task<IActionResult> DeleteFile(int id, CancellationToken cancellationToken)
    {
        var command = new DeleteFileCommand {Id = id};
        await Mediator.Send(command, cancellationToken);

        return Ok();
    }
}