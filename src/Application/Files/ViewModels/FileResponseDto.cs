using FlyingDonkeyFileUploader.Application.Common.Mappings;
using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Application.Files.ViewModels;

public class FileResponseDto : IMapFrom<File>
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string MimeType { get; set; }
    public string Extension { get; set; }
    public long Size { get; set; }
    public DateTime Created { get; set; }
}