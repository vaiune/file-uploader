export interface PaginationModel<T> {
  totalCount: number,
  pageNumber: number,
  totalPages: number,
  items: T[]
}