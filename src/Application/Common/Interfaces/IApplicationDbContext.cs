﻿using FlyingDonkeyFileUploader.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Application.Common.Interfaces;

public interface IApplicationDbContext
{
    DbSet<File> Files { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}
