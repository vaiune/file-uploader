using FlyingDonkeyFileUploader.Application.Common.Interfaces;
using FlyingDonkeyFileUploader.Application.Files.ViewModels;
using MediatR;

namespace FlyingDonkeyFileUploader.Application.Files.Queries;

public class GetAllFilesQuery : IRequest<IEnumerable<FileResponseDto>> { }

public class GetAllFilesQueryHandler : IRequestHandler<GetAllFilesQuery, IEnumerable<FileResponseDto>>
{
    private readonly IFileService _fileService;

    public GetAllFilesQueryHandler(IFileService fileService)
    {
        _fileService = fileService;
    }

    public async Task<IEnumerable<FileResponseDto>> Handle(GetAllFilesQuery request, CancellationToken cancellationToken)
    {
        return await _fileService.GetAllFiles(cancellationToken);
    }
}