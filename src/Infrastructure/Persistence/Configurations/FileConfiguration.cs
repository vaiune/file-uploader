using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Infrastructure.Persistence.Configurations;

public class FileConfiguration : IEntityTypeConfiguration<File>
{
    public void Configure(EntityTypeBuilder<File> builder)
    {
        builder.Property(f => f.Id)
            .ValueGeneratedOnAdd();

        builder.Property(f => f.Name)
            .HasMaxLength(300)
            .IsRequired();

        builder.Property(f => f.Size)
            .IsRequired();

        builder.Property(f => f.MimeType)
            .IsRequired();
        
        builder.Property(f => f.Extension)
            .IsRequired();
    }
}