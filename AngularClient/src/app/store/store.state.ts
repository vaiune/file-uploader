import { ExtensionModel } from "../models/extension.model";
import { FileModel } from "../models/file.model";

export interface AppState {
  files: FileModel[],
  totalCount: number,
  pageNumber: number,
  totalPages: number,

  extensions: ExtensionModel[],
  error: any,
  userState: UserState,

  isLoading: boolean
}

export interface UserState {
  currentPage: number,
  pageSize: number,
  extension: string
}