using AutoMapper;
using AutoMapper.QueryableExtensions;
using FlyingDonkeyFileUploader.Application.Common.Interfaces;
using FlyingDonkeyFileUploader.Application.Common.Mappings;
using FlyingDonkeyFileUploader.Application.Common.Models;
using FlyingDonkeyFileUploader.Application.Files.Queries;
using FlyingDonkeyFileUploader.Application.Files.ViewModels;
using FlyingDonkeyFileUploader.Domain.Events;
using Microsoft.EntityFrameworkCore;
using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Infrastructure.Services;

public class FileDiskService : IFileService
{
    private readonly string _uploadedFilesPath = Path.Combine(Directory.GetCurrentDirectory(), "UploadedFiles");
    private readonly IApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public FileDiskService(IApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
        
        Directory.CreateDirectory(_uploadedFilesPath);
    }

    public async Task<IEnumerable<FileResponseDto>> GetAllFiles(CancellationToken ct)
    {
        return await _dbContext.Files
            .ProjectTo<FileResponseDto>(_mapper.ConfigurationProvider)
            .ToListAsync(ct);
    }

    public async Task<PaginatedList<FileResponseDto>> GetPaginatedFiles(GetFilesPaginatedByTypeQuery query)
    {
        IQueryable<File> files = _dbContext.Files.OrderBy(i => i.Id);
        
        if (!string.IsNullOrEmpty(query.Extension))
            files = files.Where(x => x.Extension == query.Extension);
        
        return await files.ProjectTo<FileResponseDto>(_mapper.ConfigurationProvider)
            .PaginatedListAsync(query.PageNumber, query.PageSize);
    }

    public async Task<int> AddFile(FileRequestDto fileRequest, CancellationToken ct)
    {
        var newFile = new File();
        newFile.Name = fileRequest.Name;
        newFile.Extension = fileRequest.Extension;
        newFile.Size = fileRequest.Size;
        newFile.MimeType = fileRequest.MimeType;
        newFile.AddDomainEvent(new FileUploadedEvent(newFile));

        await _dbContext.Files.AddAsync(newFile, ct);
        await _dbContext.SaveChangesAsync(ct);

        // Saves to disk
        var filePath = Path.Combine(_uploadedFilesPath, newFile.Id + newFile.Extension);
        await System.IO.File.WriteAllBytesAsync(filePath, fileRequest.Content, ct);
        
        return newFile.Id;
    }

    public async Task<FileResultDto> GetFileById(int id)
    {
        var storedFile = await _dbContext.Files.FindAsync(id);
        var result = _mapper.Map<FileResultDto>(storedFile);
        
        if (storedFile is not null)
        {
            var filePath = Path.Combine(_uploadedFilesPath, storedFile.Id + storedFile.Extension);
            if (System.IO.File.Exists(filePath))
            {
                result.Content = await System.IO.File.ReadAllBytesAsync(filePath);
                result.Successful = true;
                return result;
            }
        }

        result.Successful = false;
        return result;
    }

    public async Task<IEnumerable<FileTypeDto>> GetFileTypes(CancellationToken ct)
    {
        var files = await _dbContext.Files.Select(i => i.Extension).Distinct().ToListAsync(cancellationToken: ct);
        var fileTypes = new List<FileTypeDto>();
        
        foreach (var item in files)
        {
            fileTypes.Add(new FileTypeDto()
            {
                Extension = item,
                Count = await _dbContext.Files.CountAsync(i => i.Extension == item, cancellationToken: ct)
            });
        }

        return fileTypes;
    }

    public async Task DeleteFile(int id, CancellationToken ct)
    {
        var existing = await _dbContext.Files.FindAsync(id);

        if (existing is not null)
        {
            _dbContext.Files.Remove(existing);
            await _dbContext.SaveChangesAsync(ct);
            
            var filePath = Path.Combine(_uploadedFilesPath, existing.Id + existing.Extension);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }
    }
}