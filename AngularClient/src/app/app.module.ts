import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap/modal';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { appReducer } from './store/store.reducer';
import { CommonModule } from '@angular/common';
import { FileComponent } from './file/file.component';
import { EffectsModule } from '@ngrx/effects';
import { StateEffects } from './store/store.effects';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { MainComponent } from './main/main.component';
import { UploadComponent } from './upload/upload.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { FileSizePipe } from './pipes/file-size.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    FileComponent,
    MainComponent,
    UploadComponent,
    FileSizePipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    AlertModule.forRoot(),
    EffectsModule.forRoot([StateEffects]),
    StoreModule.forRoot({ state: appReducer })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
