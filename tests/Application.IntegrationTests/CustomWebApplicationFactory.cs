﻿using FlyingDonkeyFileUploader.Infrastructure.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FlyingDonkeyFileUploader.Application.IntegrationTests;

public class CustomWebApplicationFactory : WebApplicationFactory<Program>
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        // builder.ConfigureAppConfiguration(configurationBuilder =>
        // {
        //     var integrationConfig = new ConfigurationBuilder()
        //         .AddJsonFile("appsettings.json")
        //         .AddEnvironmentVariables()
        //         .Build();
        //
        //     configurationBuilder.AddConfiguration(integrationConfig);
        // });

        builder.ConfigureServices((builder, services) =>
        {
            builder.Configuration["sqliteConnection"] = $"Data Source=/tmp/{Guid.NewGuid().ToString()}.db";
            
            services
                .Remove<DbContextOptions<ApplicationDbContext>>()
                .AddDbContext<ApplicationDbContext>((sp, options) =>
                    options.UseSqlite(builder.Configuration["sqliteConnection"],
                        builder => builder.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
        });
    }
}
