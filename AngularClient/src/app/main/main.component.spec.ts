import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { selectError } from '../store/store.selectors';

import { MainComponent } from './main.component';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;
  let store: MockStore;
  let mockSelector: any;
  
  afterEach(() => {
    store?.resetSelectors();
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainComponent ],
      providers: [
        provideMockStore()
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
  });

  it('should create', () => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should has error', () => {
    mockSelector = store.overrideSelector(selectError, 'error message');
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const errorEl = fixture.nativeElement.querySelector("#error");
    
    expect(errorEl).toBeTruthy();
  });

  it('should not has error', () => {
    mockSelector = store.overrideSelector(selectError, undefined);
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const errorEl = fixture.nativeElement.querySelector("#error");
    
    expect(errorEl).toBeFalsy();
  });

  it('should errors match', () => {
    const errorMessage = 'my error message';
    mockSelector = store.overrideSelector(selectError, errorMessage);
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const errorEl = fixture.nativeElement.querySelector("#error");
    
    expect(errorEl.textContent).toContain(errorMessage)
  });

});
