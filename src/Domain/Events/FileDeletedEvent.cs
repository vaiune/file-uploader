using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Domain.Events;

public class FileDeletedEvent : BaseEvent
{
    public FileDeletedEvent(File file)
    {
        File = file;
    }

    public File File { get; }
}