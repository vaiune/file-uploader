import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, mergeMap, catchError, tap, withLatestFrom } from 'rxjs/operators';
import { FileService } from '../services/file.service';
import { catchNetworkError, deleteFile, loadExtensions, loadExtensionsCompleted, loadFiles, loadFilesCompleted, setExtension, setPage, uploadFile, uploadFileCompleted } from './store.actions';
import { selectUserState } from './store.selectors';
import { AppState } from './store.state';
 
@Injectable()
export class StateEffects {
 
  loadFiles$ = createEffect(() => this.actions$.pipe(
    ofType(loadFiles),
    withLatestFrom(this.store.select(selectUserState)),
    tap(() => console.log('called')),
    mergeMap(([action, state]) => this.fileService.getFiles(state.currentPage, state.pageSize, state.extension)
      .pipe(
        map(files => loadFilesCompleted({ payload: files })),
        catchError((e) => of(catchNetworkError({ e: e.message })))
      ))
    )
  )

  loadExtensions$ = createEffect(() => this.actions$.pipe(
    ofType(loadExtensions),
    mergeMap((action) => this.fileService.getExtensions()
      .pipe(
        map(extensions => loadExtensionsCompleted({ payload: extensions })),
        catchError((e) => of(catchNetworkError({ e: e.message })))
      ))
    )
  )

  upload$ = createEffect(() => this.actions$.pipe(
    ofType(uploadFile),
    mergeMap((action) => this.fileService.upload(action.file)
      .pipe(
        map(() => uploadFileCompleted()),
        catchError((e) => of(catchNetworkError({ e: e.message })))
      ))
    )
  )

  delete$ = createEffect(() => this.actions$.pipe(
    ofType(deleteFile),
    mergeMap((action) => this.fileService.delete(action.id)
      .pipe(
        map(() => uploadFileCompleted()),
        catchError((e) => of(catchNetworkError({ e: e.message })))
      ))
    )
  )

  uploadCompletedLoadFiles$ = createEffect(() => this.actions$.pipe(
    ofType(uploadFileCompleted),
    map(() => loadFiles()))
  )

  uploadCompletedLoadExtensions$ = createEffect(() => this.actions$.pipe(
    ofType(uploadFileCompleted),
    map(() => loadExtensions()))
  )
 
  setPage$ = createEffect(() => this.actions$.pipe(
    ofType(setPage),
    map(() => loadFiles()))
  )
 
  setExtensions$ = createEffect(() => this.actions$.pipe(
    ofType(setExtension),
    map(() => loadFiles()))
  )

  uploadCompleted$ = createEffect(() => this.actions$.pipe(
    ofType(),
    map(() => loadFiles()))
  )

  constructor(
    private store: Store<{ state: AppState }>,
    private actions$: Actions,
    private fileService: FileService
  ) {
  }
}