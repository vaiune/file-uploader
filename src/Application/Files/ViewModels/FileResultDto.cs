using FlyingDonkeyFileUploader.Application.Common.Mappings;
using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Application.Files.ViewModels;

public class FileResultDto : IMapFrom<File>
{
    public bool Successful { get; set; }
    public string Name { get; set; }
    public string MimeType { get; set; }
    public byte[] Content { get; set; }
}