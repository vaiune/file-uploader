using FlyingDonkeyFileUploader.Application.Common.Interfaces;
using MediatR;

namespace FlyingDonkeyFileUploader.Application.Files.Commands;

public class DeleteFileCommand : IRequest
{
    public int Id { get; init; }
}

public class DeleteFileCommandHandler : IRequestHandler<DeleteFileCommand>
{
    private readonly IFileService _fileService;

    public DeleteFileCommandHandler(IFileService fileService)
    {
        _fileService = fileService;
    }
    
    public async Task<Unit> Handle(DeleteFileCommand request, CancellationToken cancellationToken)
    {
        await _fileService.DeleteFile(request.Id, cancellationToken);
        
        return Unit.Value;
    }
}