import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { uploadFile } from '../store/store.actions';
import { AppState } from '../store/store.state';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  selectedFile: File
  imgSrc: string

  constructor(private store: Store<{ state: AppState }>) { }

  ngOnInit(): void {
  }

  uploadFile(files: FileList) {
    if (files.length === 0)
      return;

    this.selectedFile = files[0]
    if (this.selectedFile.type.includes('image')) {
      const reader = new FileReader();
      reader.onload = e => this.imgSrc = reader.result.toString();

      reader.readAsDataURL(this.selectedFile);
    }

    this.store.dispatch(uploadFile({ file: this.selectedFile }))
  }
}
