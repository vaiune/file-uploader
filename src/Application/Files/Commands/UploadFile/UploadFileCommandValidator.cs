using FluentValidation;

namespace FlyingDonkeyFileUploader.Application.Files.Commands.UploadFile;

public class UploadFileCommandValidator : AbstractValidator<UploadFileCommand>
{
    public UploadFileCommandValidator()
    {
        RuleFor(x => x.FileRequest).NotNull()
            .DependentRules(() =>
            {
                RuleFor(x => x.FileRequest.Name).NotEmpty().WithMessage("File name is required.");
                
                RuleFor(x => x.FileRequest.MimeType).NotEmpty().WithMessage("File extension is required.");
                
                RuleFor(x => x.FileRequest.Size).GreaterThan(0).WithMessage("File size must be greater than 0.");
                
                RuleFor(x => x.FileRequest.Content).NotEmpty().WithMessage("File payload must be provided.");
            })
            .WithMessage("Upload payload is required.");
    }
}