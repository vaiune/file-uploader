﻿using FlyingDonkeyFileUploader.Application.Common.Exceptions;
using FluentAssertions;
using FluentValidation.Results;
using NUnit.Framework;

namespace FlyingDonkeyFileUploader.Application.UnitTests.Common.Exceptions;

public class ValidationExceptionTests
{
    [Test]
    public void SingleValidationFailureCreatesASingleElementErrorDictionary()
    {
        var failures = new List<ValidationFailure>
            {
                new ValidationFailure("FileName", "must be provided"),
            };

        var actual = new ValidationException(failures).Errors;

        actual.Keys.Should().BeEquivalentTo(new string[] { "FileName" });
        actual["FileName"].Should().BeEquivalentTo(new string[] { "must be provided"});
    }

    [Test]
    public void MulitpleValidationFailureForMultiplePropertiesCreatesAMultipleElementErrorDictionaryEachWithMultipleValues()
    {
        var failures = new List<ValidationFailure>
            {
                new ValidationFailure("FileName", "must be provided"),
                new ValidationFailure("FileSize", "must be greater than 1 KB"),
                new ValidationFailure("ContentType", "must be one of the image, application, text mime-types"),
            };

        var actual = new ValidationException(failures).Errors;

        actual.Keys.Should().BeEquivalentTo(new string[] { "FileName", "FileSize", "ContentType" });

        actual["FileName"].Should().BeEquivalentTo(new string[] {"must be provided"});
        actual["FileSize"].Should().BeEquivalentTo(new string[] {"must be greater than 1 KB"});
        actual["ContentType"].Should().BeEquivalentTo(new string[] {"must be one of the image, application, text mime-types"});
    }
}
