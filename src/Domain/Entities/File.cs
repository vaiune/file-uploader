namespace FlyingDonkeyFileUploader.Domain.Entities;

public class File : BaseAuditableEntity
{
    public string Name { get; set; }
    public string MimeType { get; set; }
    public string Extension { get; set; }
    public long Size { get; set; }
}