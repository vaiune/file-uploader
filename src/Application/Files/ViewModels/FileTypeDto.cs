namespace FlyingDonkeyFileUploader.Application.Files.ViewModels;

public class FileTypeDto
{
    public string Extension { get; set; }
    public int Count { get; set; }
}