using File = FlyingDonkeyFileUploader.Domain.Entities.File;

namespace FlyingDonkeyFileUploader.Domain.Events;

public class FileUploadedEvent : BaseEvent
{
    public FileUploadedEvent(File file)
    {
        File = file;
    }

    public File File { get; }
}