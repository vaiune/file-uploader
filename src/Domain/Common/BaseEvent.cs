﻿using MediatR;

namespace FlyingDonkeyFileUploader.Domain.Common;

public abstract class BaseEvent : INotification
{
}
