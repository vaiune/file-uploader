import { AppState } from "./store.state";
import { createReducer, on } from '@ngrx/store';
import { catchNetworkError, loadExtensionsCompleted, loadFiles, loadFilesCompleted, setExtension, setPage } from "./store.actions";

const initialState: AppState = {
  files: [],
  pageNumber: 0,
  totalCount: 0,
  totalPages: 0,

  userState: {
    currentPage: 1,
    extension: '',
    pageSize: 10
  },

  extensions: [],
  error: undefined,

  isLoading: false
}

export const appReducer = createReducer(
  initialState,
  on(loadFiles, (state) => ({
    ...state,
    isLoading: true
  })),
  on(setPage, (state, action) => ({
    ...state,
    userState: {
      ...state.userState,
      currentPage: action.page
    }
  })),
  on(setExtension, (state, action) => ({
    ...state,
    userState: {
      ...state.userState,
      extension: action.extension
    }
  })),
  on(loadFilesCompleted, (state, action) => ({
    ...state,

    files: action.payload.items,
    pageNumber: action.payload.pageNumber,
    totalCount: action.payload.totalCount,
    totalPages: action.payload.totalPages,
    isLoading: false
  })),
  on(loadExtensionsCompleted, (state, action) => ({
    ...state,
    extensions: action.payload
  })),
  on(catchNetworkError, (state, action) => ({
    ...state,
    error: action.e
  }))
);